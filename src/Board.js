import React from 'react';
import './Board.css';
import Flashcard from './Flashcard'

function Board() {
  let phrases;
  phrases = ['はい', 'いいえ', 'おねがいします'];


  let handlePhrase = (value) => {
    console.log(value);
  };

  return (
    <div className='board'>
      {
        phrases.map((phrase, index) => <Flashcard onSelectLanguage={handlePhrase} key={index} phrase={phrase}/>)
      }
    </div>
  );
}

export default Board;
