import React from 'react';
import './Flashcard.css';

function Flashcard(props) {
  let child = (value) => {
    props.onSelectLanguage(value)
  };

  return (
    <div className='phrase' tabIndex={1} onClick={(event => {child('df')})}>{props.phrase}</div>
  );
}

export default Flashcard;
